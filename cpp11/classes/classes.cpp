#include <iostream>
#include <set>
#include <string>
#include <vector>

using namespace std;

class Gadget
{
	int id_{-1};
	string name_;
public:
	Gadget(int id, const string& name) : id_{id}, name_{name}
	{}

	Gadget(int id) : Gadget{id, "not-set"}
	{		
	}

	Gadget() = default;

	int id() const
	{
		return id_;
	}
};

class NoCopyable
{
public:
	NoCopyable() = default;
	NoCopyable(const NoCopyable&) = delete;
	NoCopyable& operator=(const NoCopyable&) = delete;
};

void foo(int d)
{
	cout << "foo(int: " << d << ")" << endl;
}

void foo(double) = delete;

namespace SFINAE
{

	template <typename T>
	void foo(T arg,
		typename enable_if<is_floating_point<T>::value>::type* ptr = nullptr)
	{
		cout << "foo(T: " << arg << ")" << endl;
	}

	template <typename T>
	struct IsVoid : std::bool_constant<false>
	{
	};

	template <>
	struct IsVoid<void> : std::bool_constant<true>
	{
	};
}

template <typename Key>
class IndexedSet : public set<Key>
{
public:
	using set<Key>::set; // constructor inheritance

	IndexedSet(const IndexedSet&) = delete;
	IndexedSet& operator=(const IndexedSet&) = delete;

	const Key& operator[](size_t index) const
	{
		auto it = begin();

		it = next(it, index);

		return *it;
	}
};



int main()
{
	Gadget g1;

	cout << "g1.id() = " << g1.id() << endl;

	NoCopyable nc1;
	//NoCopyable nc2 = nc1;

	foo(89);
	//foo(3.14F);
	//foo("str"s);

	IndexedSet<string> mset = { "1_one", "3_three", "2_two" };
	//IndexedSet<string> cp = mset;

	cout << "items: ";
	for (const auto& item : mset)
		cout << item << " ";
	cout << endl;

	cout << mset[1] << endl;

	vector<Gadget> gadgets;

	gadgets.push_back({ 1, "pokemon" });
	gadgets.emplace_back(2, "evil pokemon");
	gadgets.emplace_back(Gadget{ 1, "pokemon" });

	system("PAUSE");
}