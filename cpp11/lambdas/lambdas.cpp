#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
#include <queue>
#include <memory>

using namespace std;

class Magic_234234273472345
{
public:
	bool operator()(int x) const { return x % 2 == 0; }
};

class Magic_2378452783452735
{
	const int base;
	int& pos;
public:
	Magic_2378452783452735(int base, int& pos) : base(base), pos(pos)
	{}

	bool operator()(int x) const {
		if (x % base == 0)
			return true;
		else
		{
			++pos;
			return false;
		}
	}
};

class Magic_9347562934629
{
	int seed;
public:
	Magic_9347562934629(int seed) : seed{seed}
	{}

	int operator()()
	{
		return ++seed;
	}
};

function<int()> create_generator(int seed)
{
	return [seed]() mutable { return ++seed; };
}

using WorkItem = function<void()>;
using WorkQueue = queue<WorkItem>;

double get_temp(int id)
{
	return 34.3;
}

class Worker
{
public:
	void operator()()
	{
		cout << "Worker works..." << endl;
	}
};

class Printer
{
public:
	void print() const
	{
		cout << "Printing...";
	}
};

int main()
{
	vector<int> vec = { 1, 2, 3, 4, 5, 6 };

	auto is_even = [](int x) { return x % 2 == 0; };

	auto it1 = find_if(vec.begin(), vec.end(), is_even);
	auto it2 = find_if(vec.rbegin(), vec.rend(), is_even);

	//auto it = find_if(vec.begin(), vec.end(), Magic_234234273472345{});

	int base = 3;
	int pos = 0;

	auto it3 = find_if(vec.begin(), vec.end(), 
		[base, &pos](int x) {
			if (x % base == 0)
				return true;
			else
			{
				++pos;
				return false;
			}
	});

	cout << "Value: " << *it3 << "; Pos: " << pos << endl;

	vector<int> numbers(10);
	generate_n(numbers.begin(), 10, create_generator(100));

	cout << "numbers: ";
	for (const auto& item : numbers)
		cout << item << " ";
	cout << endl;

	WorkQueue q;
	vector<double> temp_values;

	unique_ptr<Printer> prn = make_unique<Printer>();

	auto print_job = [prn = move(prn)]{ prn->print(); };

	q.push([] { cout << "Start" << endl; });
	q.push([&temp_values] { temp_values.push_back(get_temp(8)); });
	//q.push(move(print_job));
	q.push(Worker{});
	q.push([] { cout << "End" << endl; });

	//----

	while(!q.empty())
	{
		WorkItem work_item = q.front();
		work_item(); // call
		q.pop();
	}

	// recursive lambda
	function<int(int)> fib =
		[&fib](int n) { return (n <= 2) ? 1 : fib(n - 1) + fib(n - 2); };
		
	cout << fib(22) << endl;

	system("PAUSE");
}