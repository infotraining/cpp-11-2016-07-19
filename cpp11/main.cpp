#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <array>
#include <functional>
#include <algorithm>
#include <cassert>
#include "catch.hpp"

using namespace std;

namespace Legacy
{
	void foo(int* ptr)
	{
		if (ptr == NULL)
		{
			cout << "ptr = 0" << endl;
		}
		else
		{
			cout << "*ptr: " << *ptr << endl;
		}
	}

	void foo(int value)
	{
		cout << "foo(int: " << value << ")" << endl;
	}
}

namespace Cpp11
{
	void foo(int* ptr)
	{
		if (ptr == nullptr)
		{
			cout << "ptr = 0" << endl;
		}
		else
		{
			cout << "*ptr: " << *ptr << endl;
		}
	}

	void foo(int value)
	{
		cout << "foo(int: " << value << ")" << endl;
	}

	void foo(nullptr_t)
	{
		cout << "foo(nullptr_t)" << endl;
	}
}

template <typename Arg>
void forward_with_pointer(Arg arg)
{
	Cpp11::foo(arg);
}

TEST_CASE("nullptr")
{
	SECTION("legacy NULL can be bound to functions with int args")
	{
		Legacy::foo(NULL);
	}

	SECTION("replaces NULL constant in assignment")
	{
		int* ptr1 = NULL;
		REQUIRE(ptr1 == nullptr);

		int* ptr2 = nullptr; 
		REQUIRE(ptr2 == nullptr);

		int* ptr3{};
		REQUIRE(ptr3 == nullptr);
	}

	SECTION("is safer as argument passed to function")
	{
		int* ptr = nullptr;
		Cpp11::foo(ptr);
		Cpp11::foo(nullptr);
	}

	SECTION("can be safely passed to template")
	{
		forward_with_pointer(nullptr);
	}
}

TEST_CASE("literal strings")
{
	string str = "text"s;	
}

template <typename T>
void foo(const T& arg)
{
	
}

void my_function(int)
{}

TEST_CASE("auto")
{
	SECTION("compiler deduces type from init expr")
	{
		auto i = 42;
		auto ptr = &i;
	}

	SECTION("can be used with pointers")
	{
		auto i = 42;

		const auto* cptr1 = &i; // const int* cptr1		
		const auto cptr2 = &i; // int* const cptr2
	}

	SECTION("const can be added")
	{
		const auto i = 42;

		auto ptr = &i;
	}

	int x = 12;
	const int& crx = x;

	SECTION("auto without ref or ptr")
	{		
		SECTION("reference and const modifiers are removed")
		{
			auto ax = crx; // int
		}

		int tab[10];
		const int ctab[10] = {};

		SECTION("tables decay to pointers")
		{
			auto atab = tab; // int*
			auto actab = ctab; // const int*
		}

		SECTION("function decays to function pointer")
		{
			auto f = my_function; // void (*f)(int)
		}
	}

	SECTION("auto with ref")
	{
		SECTION("ref and conts or volatiles are preserved")
		{
			auto& ax = crx;
		}

		int tab[10];
		const int ctab[10] = {};

		SECTION("arrays don't decay")
		{
			auto& atab = tab; // int (&atab)[10]
			auto& actab = ctab; // const (&actab)[10]
		}

		SECTION("functions don't decay to pointers")
		{
			auto& af = my_function;
		}
	}

	SECTION("init syntax")
	{
		int i = 10;

		SECTION("direct init")
		{			
			auto ai(i);
			auto ai2{i};  // C++17 the same as line above
		}

		SECTION("copy syntax")
		{
			auto ai = i;
		}
	}

	SECTION("with braces is init_list")
	{
		auto items = { 1, 2, 3 }; // initializer_list<int>
	}
}

TEST_CASE("range - based - for")
{
	SECTION("iterates over containers")
	{
		vector<int> vec = { 1, 2, 3, 4 };

		cout << "items: ";
		for(const auto& item : vec)
		{
			cout << item << " ";
		}
		cout << endl;
	}

	SECTION("iterates over native arrays")
	{
		int tab[10] = { 1, 2, 3 };

		cout << "items: ";
		for(const auto& item : tab)
		{
			cout << item << " ";
		}
		cout << endl;
	}

	SECTION("iterates over initializer list")
	{
		cout << "items: ";
		for(const auto& item : {1, 2, 3})
		{
			cout << item << " ";
		}
		cout << endl;
	}

	SECTION("C++17")
	{
		vector<int> container = { 1, 2, 3 };

		//for (item : container)
		//{
		//	cout << item << endl;
		//}

		for(auto&& item : container)
		{
			cout << item << endl;
		}
	}
}

TEST_CASE("scoped-enums")
{
	enum class Weed : uint8_t { perz, skrzyp, trawa, inne };

	Weed w = Weed::inne;

	int x = static_cast<int>(w);

	cout << "weed x: " << x << endl;

	w = static_cast<Weed>(2);

	REQUIRE(w == Weed::trawa);
}

class Vector2D
{
	int x_, y_;
public:
	Vector2D(int x, int y) : x_{ x }, y_{ y }
	{
		cout << "Vector2D(x: " << x_ << ", y: " << y_ << ")" << endl;
	}
};

template <typename T>
class Queue
{
	deque<T> dq_;

public:
	using iterator = typename deque<T>::iterator;
	using const_iterator = typename deque<T>::const_iterator;

	Queue(initializer_list<T> il)
	{
		for (const auto& item : il)
			dq_.push_back(item);
	}

	iterator begin()
	{
		return dq_.begin();
	}

	const_iterator begin() const
	{
		return dq_.begin();
	}

	const_iterator cbegin() const
	{
		return dq_.cbegin();
	}

	iterator end()
	{
		return dq_.end();
	}

	const_iterator end() const
	{
		return dq_.end();
	}

	const_iterator cend() const
	{
		return dq_.cend();
	}
};

TEST_CASE("uniform init syntax")
{
	int y{};

	int x{ 10 };
	// char c{ x }; // compilation error - narrowing conversion

	Vector2D v1{ 1, 2 };
	auto v2 = Vector2D{ 5, 6 };
	Vector2D v3 = { 8, 9 };

	int tab1[] = { 5, 6, 23, static_cast<int>(76.4) };

	Queue<int> q = { 1, 2, 3, 4 };

	cout << "q: ";
	for(const auto& item : q)
	{
		cout << item << " ";
	}
	cout << endl;

	vector<int> vec1(10, 1);
	REQUIRE(vec1.size() == 10);

	vector<int> vec2{ 10, 1 };
	REQUIRE(vec2.size() == 2);
	vector<string> vec3{ 10, "one" };
}

template <typename Callable>
auto call(Callable c) -> decltype(c())
{
	return c();
}

template <typename Vector>
auto multiply(const Vector& v1, const Vector& v2) // -> decltype(v1 * v2)
{
	return v1 * v2;
}

TEST_CASE("decltype")
{
	map<string, int> dict = { {"one", 1}, {"two", 2}, {"three", 3} };

	SECTION("auto vs decltype")
	{
		auto d2 = dict;
		REQUIRE(d2.size() == 3);

		decltype(d2) d3;
		REQUIRE(d3.size() == 0);
	}
}

TEST_CASE("set with lambda comparer")
{
	auto compare_by_val = [](int* ptr1, int* ptr2) { return *ptr1 < *ptr2; };

	set<int*, decltype(compare_by_val)> mset(compare_by_val);
	mset.insert({ new int{7}, new int{9}, new int{2} });

	cout << "items: ";
	for (const auto& ptr_item : mset)
		cout << *ptr_item << " ";
	cout << endl;

	for (const auto& ptr_item : mset)
		delete ptr_item;
}

namespace Legacy
{
	template <typename T>
	struct IteratorFor
	{
		typedef typename T::iterator type;
	};

	template <typename T, std::size_t N>
	struct IteratorFor<T(&)[N]>
	{
		typedef T* type;
	};	

	template <typename Container>
	typename IteratorFor<Container>::type find_null(Container& c) 
	{
		//...
	}
}

template <typename Container>
auto find_null(Container& container) -> decltype(begin(container))
{
	auto it = begin(container);

	for (; it != end(container); ++it)
		if (*it == nullptr)
			return it;

	return it;
}

TEST_CASE("find_null exercise")
{
	SECTION("works with vector of pointers")
	{
		vector<int*> ptrs = { new int{9}, new int{9}, nullptr, new int{10} };

		auto where_null = find_null(ptrs);

		REQUIRE(*where_null == nullptr);
		REQUIRE(distance(ptrs.begin(), where_null) == 2);

		for (const auto& ptr : ptrs)
			delete ptr;
	}

	SECTION("works with il of shared_ptr")
	{
		auto il = { make_shared<int>(10), shared_ptr<int>{}, make_shared<int>(11) };

		auto where_null = find_null(il);

		REQUIRE(distance(il.begin(), where_null) == 1);
	}

	SECTION("where all pointer are not null end iterator is returned")
	{
		vector<int*> ptrs = { new int{ 9 }, new int{ 9 }, new int{ 10 } };

		auto where_null = find_null(ptrs);

		REQUIRE(where_null == ptrs.end());
	}
}