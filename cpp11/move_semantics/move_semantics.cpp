#include <iostream>
#include <string>
#include <vector>

using namespace std;

int generate()
{
	static int seed = 0;
	return ++seed;
}

struct X
{
	int id;

	X(int id) : id{id}
	{
		cout << "X(" << id << ")" << endl;
	}

	~X()
	{
		cout << "~X(" << id << ")" << endl;
	}
};

template <typename T>
class Vector
{
	// impl
public:
	void push_back(const T& item) { cout << "v.push_back(const T&)" << endl; };
	void push_back(T&& item) { cout << "v.push_back(T&&)" << endl; }
};

X get()
{
	return { 10 };
}

template <typename T>
void optimized_impl(T& obj)
{
	static_assert(!is_const<T>::value, "T cannot be const");

	T temp = move(obj);
}

class Array
{
	string name_;
	size_t size_;
	int* data_;

public:
	Array(const string& name, size_t size, int value = 0) 
		: name_{ name }, size_{ size }, data_{ new int[size_] }
	{
		fill_n(data_, size_, value);
		cout << "Array(name: " << name_ << ", size: " << size_ << ")" << endl;
	}

	/*Array(const Array&) = delete;
	Array& operator=(const Array&) = delete;*/

	Array(const Array& source) : name_{source.name_}, size_{source.size_}, data_{new int[source.size_]}
	{
		copy(source.data_, source.data_ + size_, data_);

		cout << "Array(const Array&: name=" << name_ << ")" << endl;
	}

	Array& operator=(const Array& source)
	{
		Array temp(source);
		swap(temp);

		cout << "operator=(const Array&: name=" << name_ << ")" << endl;

		return *this;
	}

	void swap(Array& other)
	{
		std::swap(name_, other.name_);
		std::swap(size_, other.size_);
		std::swap(data_, other.data_);
	}

	Array(Array&& source) noexcept
		: name_{ move(source.name_) }, size_ { source.size_ }, data_{ source.data_ }
	{
		source.size_ = 0;
		source.data_ = nullptr;

		cout << "Array(Array&&)" << endl;
	}

	Array& operator=(Array&& source) noexcept
	{
		if (this != &source)
		{
			name_ = move(source.name_);
			size_ = source.size_;
			data_ = source.data_;

			source.size_ = 0;
			source.data_ = nullptr;

			cout << "operator=(Array&&)" << endl;
		}
		
		return *this;
	}

	~Array()
	{
		delete[] data_;

		cout << "~Array(size: " << size_ << ")" << endl;
	}

	size_t size() const
	{
		return size_;
	}
};

Array create_array(const string& name)
{
	Array arr(name, 100, 0);

	return arr;
}

void call(int x)
{
}

void call(const string& text)
{	
}

void call(string&& temp_msg)
{	
}

template <typename Arg>
void call_and_logger(Arg&& arg)
{
	call(forward<Arg>(arg)); // perfect forwarding

	cout << "Log..." << endl;
}

class HighlyOptimizedPokemon
{
	vector<int> vec;
	Array arr;
	string pokemon;
public:
	HighlyOptimizedPokemon(const string& name, size_t size) : arr(name, size)
	{}

	//HighlyOptimizedPokemon(const HighlyOptimizedPokemon&) = default;
	//HighlyOptimizedPokemon& operator=(const HighlyOptimizedPokemon&) = default;
	//HighlyOptimizedPokemon(HighlyOptimizedPokemon&&) noexcept = default;
	//HighlyOptimizedPokemon& operator=(HighlyOptimizedPokemon&&) noexcept = default;

	//~HighlyOptimizedPokemon()
	//{
	//	cout << "Ahhhhhh..." << endl;
	//}
};

int main()
{
	{
		int x = 10;
		
		int& lv_ref1 = x;

		const int& lv_ref2 = generate();

		const auto& lv_ref3 = get();

		X&& rv_ref = get();
		
		cout << "after get()" << endl;

		cout << "id: " << lv_ref3.id << endl;

		Vector<X> vecx;

		X xx{ 1 };
		vecx.push_back(xx);
		vecx.push_back(get());
		vecx.push_back(x);
		vecx.push_back(move(xx));

		xx = get();
	}

	vector<bool> evil_vector_bool = { 1, 0, 1, 0, 0, 0, 0 };

	for (auto&& bit : evil_vector_bool)
		bit.flip();

	cout << "after scope" << endl;

	cout << "\nmove doesn't move:" << endl;

	string text = "text";
	const string ctext = "ctext";

	optimized_impl(text);
	cout << "text = " << text << endl;

	//optimized_impl(ctext);
	cout << "ctext = " << ctext << endl;

	cout << "\n\n";

	{
		Array arr1("arr1", 10, 1);
		Array arr_temp = arr1;


		Array arr2 = move(arr1);
		Array arr3 = create_array("pokemon1");
		arr_temp = arr3;

		cout << "\n\n";

		vector<Array> vec;
		vec.push_back(move(arr2));
		vec.push_back(move(arr3));
		vec.push_back(create_array("pokemon2"));
		vec.emplace_back("pokemon3", 20, 9);

		cout << vec[0].size() << endl;
	}

	cout << "\n\nno to HOP..." << endl;

	{
		HighlyOptimizedPokemon pokemon{ "poki1", 100 };

		vector<HighlyOptimizedPokemon> vec;

		vec.push_back(pokemon);
		vec.push_back(move(pokemon));
		vec.push_back(HighlyOptimizedPokemon("poki2", 200));
	}

	system("PAUSE");
}