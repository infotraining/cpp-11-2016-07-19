#include <iostream>
#include <string>
#include <memory>

using namespace std;

class Socket
{
public:
	void read()
	{
		cout << "reading from socket..." << endl;
	}

	void close()
	{
		cout << "closing socket..." << endl;
	}
};

class Gadget
{
public:
	Gadget()
	{
		cout << "Gadget()" << endl;
	}

	~Gadget()
	{
		cout << "~Gadget()" << endl;
	}

	void use()
	{
		cout << "Using gadget..." << endl;
	}
};

void use(unique_ptr<Gadget> g)
{
	g->use();
}

void use(Gadget* g)
{
	if (g)
		g->use();
}

class Human
{
public:
	Human(const std::string& name) : name_(name)
	{
		std::cout << "Konstruktor Human(" << name_ << ")" << std::endl;
	}

	~Human()
	{
		std::cout << "Desktruktor ~Human(" << name_ << ")" << std::endl;
	}

	void set_partner(std::weak_ptr<Human> partner)
	{
		partner_ = partner;
	}

	void description() const
	{
		std::cout << "My name is " << name_ << std::endl;

		shared_ptr<Human> still_partner = partner_.lock();

		if (still_partner)
		{
			std::cout << "My partner is " << still_partner->name_ << std::endl;
		}
	}

private:
	std::weak_ptr<Human> partner_;
	std::string name_;
};

void memory_leak_demo()
{
	// RC husband == 1
	auto  husband = std::make_shared<Human>("Jan");

	// RC wife == 1
	auto wife = std::make_shared<Human>("Ewa");

	// RC wife ==2
	husband->set_partner(wife);

	// RC husband == 2
	wife->set_partner(husband);

	husband->description();
}

int main()
{
	{
		Socket socket;
		
		{
			auto socket_closer = [](Socket* s) { s->close(); };
			unique_ptr<Socket, decltype(socket_closer)> safe_socket(&socket, socket_closer);

			safe_socket->read();
		}

		{
			shared_ptr<Socket> safe_socket{ &socket, [](Socket* s) { s->close(); } };
			safe_socket->read();
		}
	}

	auto gp1 = make_unique<Gadget>();

	use(gp1.get());
	use(move(gp1));
	
	memory_leak_demo();

	system("PAUSE");
}