#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <chrono>
#include <future>

using namespace std;

void background_work(int id, chrono::milliseconds interval)
{
	cout << "Starting a thread: " << id << endl;

	for(int i = 0; i < 10; ++i)
	{
		cout << "THD#" << id << " is working" << endl;
		this_thread::sleep_for(interval);
	}

	cout << "End of thread: " << id << endl;
}

enum class ThreadAction { join, detach };

class ThreadGuard
{
public:
	ThreadGuard(ThreadAction action, std::thread&& thd)
		: thd_{ std::move(thd) }, action_{ action }
	{}

	template <typename... T>
	ThreadGuard(ThreadAction action, T&&... args)
		: action_{ action }, thd_{ std::forward<T>(args)... }
	{}

	ThreadGuard(const ThreadGuard&) = delete;
	ThreadGuard& operator=(const ThreadGuard&) = delete;

	ThreadGuard(ThreadGuard&&) = default;
	ThreadGuard& operator=(ThreadGuard&&) = default;

	~ThreadGuard()
	{
		if (thd_.joinable())
		{
			if (action_ == ThreadAction::join)
				thd_.join();
			else
				thd_.detach();
		}
	}

	std::thread& get()
	{
		return thd_;
	}

private:
	std::thread thd_;
	ThreadAction action_;
};

int calculate_square(int x)
{
	cout << "Start calc: " << x << endl;

	this_thread::sleep_for(chrono::milliseconds(rand() % 5000));

	return x * x;
}

void save_to_file(const string& filename)
{
	cout << "Saving to file: " << filename << endl;
	this_thread::sleep_for(chrono::milliseconds(2000));
	cout << "End of save: " << filename << endl;
}

template <typename Callable>
auto spawn_task(Callable&& f)
{
	using result_type = decltype(f());

	packaged_task<result_type()> pt(move(f));
	future<result_type> future_result = pt.get_future();

	thread thd{ move(pt) };
	thd.detach();

	return future_result;
}

int main()
{
	thread thd1{ &background_work, 1, 300ms };
	//thread thd2{ [] { background_work(2, 500ms); } };
	
	ThreadGuard g1{ ThreadAction::join,  move(thd1) };
	ThreadGuard g2{ ThreadAction::detach, &background_work, 2, 500ms };

	//thd1.join();
	//thd2.detach();

	future<int> f1 = async(launch::async, &calculate_square, 21);

	vector<future<int>> results;

	results.push_back(move(f1));

	for (int i = 100; i <= 111; ++i)
		results.push_back(async(launch::async, [i] { return calculate_square(i); }));


	cout << "Results: " << endl;
	for (auto& r : results)
		cout << r.get() << endl;

	cout << "\n\n";

	async(launch::async, &save_to_file, "data1");
	async(launch::async, &save_to_file, "data2");
	async(launch::async, &save_to_file, "data3");

	cout << "\n\n";

	auto f2 = async(launch::async, &save_to_file, "data1");
	auto f3 = async(launch::async, &save_to_file, "data2");
	auto f4 = async(launch::async, &save_to_file, "data3");

	cout << "\n\n";

	spawn_task([] { save_to_file("data1"); });
	spawn_task([] { save_to_file("data2"); });
	auto ff = spawn_task([] { save_to_file("data3"); });

	ff.wait();

	system("PAUSE");	
}