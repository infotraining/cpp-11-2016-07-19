#include <iostream>
#include <string>
#include <vector>
#include <tuple>

using namespace std;

template <int... items>
struct MaxValue;

template <int first, int... rest>
struct MaxValue<first, rest...>
{
	enum
	{
		rvalue = MaxValue<rest...>::value,
		value = (first < rvalue) ? rvalue : first
	};
};

template <int last>
struct MaxValue<last>
{
	enum
	{
		value = last
	};
};

template <size_t... indexes, typename... Ts>
auto select(const tuple<Ts...>& t)
{
	return make_tuple(get<indexes>(t)...);
}

//----------------------------------

int main()
{
	static_assert(MaxValue<1, 7, 0, -2, 9, 3>::value == 9, "Error");

	auto t1 = make_tuple(1, 3.14, "text"s, 0);
	auto t2 = select<1, 0, 2>(t1);

	cout << get<0>(t2) << ", " << get<1>(t2) << ", " << get<2>(t2) << endl;

	system("PAUSE");
}