#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <algorithm>
#include <numeric>
#include <memory>

using namespace std;

template <typename T, typename... Args>
unique_ptr<T> my_make_unique(Args&&... args)
{
	return unique_ptr<T>{new T(forward<Args>(args)...)};
}

void print()
{
	cout << endl;
}

template <typename T, typename... Args>
void print(const T& arg1, const Args&... params)
{
	cout << arg1 << " ";
	print(params...);
}

template <int... items>
struct MaxValue;

template <int first, int... rest>
struct MaxValue<first, rest...>
{
	enum
	{
		rvalue = MaxValue<rest...>::value,
		value = (first < rvalue ? rvalue : first)
	};
};

template <int last>
struct MaxValue<last>
{
	enum { value = last };
};

template <size_t... indexes, typename... Ts>
auto select(const tuple<Ts...>& t)
{
	return make_tuple(get<indexes>(t)...);
}

tuple<int, int, double> calc_stats(const vector<int>& data)
{
	auto min_it = min_element(data.begin(), data.end());
	auto max_it = max_element(data.begin(), data.end());
	auto avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

	return make_tuple(*min_it, *max_it, avg);
}

class Person
{
	string fname;
	string lname;
	bool gender;

private:
	tuple<const string&, const string&, const bool&> tied() const 
	{
		return tie(lname, fname, gender);
	}

public:
	Person(string fn, string ln) : fname{fn}, lname{ln}
	{}

	bool operator==(const Person& other)
	{
		return tied() == other.tied();
	}

	bool operator<(const Person& other)
	{
		return tied() < other.tied();
	}
};

int main()
{
	print(1, 3.14, "text"s);
	
	static_assert(MaxValue<7, 4, 12, -1, 2, 44, 0, 3>::value == 44, "Error");

	auto t1 = make_tuple(1, 3.14, "text"s, true);

	auto t2 = select<1, 0, 2>(t1);

	vector<int> data = { 5, 23, 45, 2, 1, 5, 99, 44 };

	int min, max;
	double avg;
	tie(min, max, ignore) = calc_stats(data);

	cout << "min = " << min << endl;
	cout << "max = " << max << endl;
	//cout << "avg = " << avg << endl;

	system("PAUSE");
}